# Chinese (Simplified) translation for ubuntu-docviewer-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-docviewer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-docviewer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-05 23:40+0100\n"
"PO-Revision-Date: 2019-12-03 14:39+0000\n"
"Last-Translator: Sam Zhang <sammyok@163.com>\n"
"Language-Team: Chinese (Simplified) <https://translate.ubports.com/projects/"
"ubports/docviewer-app/zh_Hans/>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-04-05 07:49+0000\n"

#: ../src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "提供的一些参数无效。"

#: ../src/app/qml/common/CommandLineProxy.qml:58
msgid "Open ubuntu-docviewer-app displaying the selected file"
msgstr "打开 ubuntu-docviewer-app 显示选中文件"

#: ../src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "全屏运行"

#: ../src/app/qml/common/CommandLineProxy.qml:71
msgid "Open ubuntu-docviewer-app in pick mode. Used for tests only."
msgstr "在选择模式下打开 ubuntu-docviewer-app，仅用测试。"

#: ../src/app/qml/common/CommandLineProxy.qml:77
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running ubuntu-docviewer-app"
msgstr "从指定的目录中显示文档，而不是 ~/Documents 目录。该目录必须在运行 ubuntu-docviewer-app 之前存在"

#: ../src/app/qml/common/DetailsPage.qml:26
#: ../src/app/qml/loView/LOViewDefaultHeader.qml:107
#: ../src/app/qml/pdfView/PdfView.qml:235
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:69
msgid "Details"
msgstr "详情"

#: ../src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "文件"

#: ../src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "位置"

#: ../src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "大小"

#: ../src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "创建日期"

#: ../src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "最近更改"

#: ../src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "MIME 类型"

#: ../src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "错误"

#: ../src/app/qml/common/ErrorDialog.qml:26
#: ../src/app/qml/common/PickImportedDialog.qml:54
#: ../src/app/qml/common/RejectedImportDialog.qml:38
#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:31
#: ../src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "关闭"

#: ../src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "多文档导入"

#: ../src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "请选择打开哪一个："

#: ../src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "不支持的文件"

#: ../src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "以下文件未导入："

#: ../src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "未知文件类型"

#: ../src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"不支持此文件。\n"
"您想把它作为纯文本文档打开吗？"

#: ../src/app/qml/common/UnknownTypeDialog.qml:38
#: ../src/app/qml/documentPage/DeleteFileDialog.qml:55
#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:28
#: ../src/app/qml/loView/LOViewGotoDialog.qml:55
#: ../src/app/qml/pdfView/PdfView.qml:180
#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "取消"

#: ../src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "同意"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: ../src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 GB"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: ../src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 MB"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: ../src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 kB"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: ../src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 比特"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "删除文件"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "删除 %1 文件"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:41
#: ../src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "您确认要永久删除这些文件吗？"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:61
#: ../src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:53
msgid "Delete"
msgstr "删除"

#: ../src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "分享"

#: ../src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr "文件未找到"

#: ../src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr "将您的设备连接到任何计算机上，简单地将文件拖拽到文档目录或者插入包含文档的可移动媒体。"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:103
#, qt-format
msgid "Today, %1"
msgstr "今天，%1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:108
#, qt-format
msgid "Yesterday, %1"
msgstr "昨天，%1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:115
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:134
msgid "yyyy/MM/dd hh:mm"
msgstr "年/月/日 小时:分钟"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:128
msgid "dddd, hh:mm"
msgstr "日期,小时:分钟"

#: ../src/app/qml/documentPage/DocumentPage.qml:23
#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:3
msgid "Documents"
msgstr "文档"

#: ../src/app/qml/documentPage/DocumentPageDefaultHeader.qml:29
msgid "Search..."
msgstr "搜索..."

#: ../src/app/qml/documentPage/DocumentPageDefaultHeader.qml:36
msgid "Sorting settings..."
msgstr "排序设置..."

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:41
msgid "Switch to single column list"
msgstr "切换到单列列表"

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:41
msgid "Switch to grid"
msgstr "切换到网格"

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:49
msgid "Pick"
msgstr "选取"

#: ../src/app/qml/documentPage/DocumentPageSearchHeader.qml:27
msgid "Back"
msgstr "返回"

#: ../src/app/qml/documentPage/DocumentPageSearchHeader.qml:47
msgid "search in documents..."
msgstr "在文档中搜索..."

#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:40
msgid "Select None"
msgstr "取消选择"

#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:40
msgid "Select All"
msgstr "全选"

#: ../src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr "未找到匹配的文档"

#: ../src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr "请确保您的查询没有拼写错误，或尝试不同的查询。"

#: ../src/app/qml/documentPage/SectionHeader.qml:13
msgid "Today"
msgstr "今天"

#: ../src/app/qml/documentPage/SectionHeader.qml:16
msgid "Yesterday"
msgstr "昨天"

#: ../src/app/qml/documentPage/SectionHeader.qml:19
msgid "Earlier this week"
msgstr "本周初"

#: ../src/app/qml/documentPage/SectionHeader.qml:22
msgid "Earlier this month"
msgstr "本月初"

#: ../src/app/qml/documentPage/SectionHeader.qml:24
msgid "Even earlier..."
msgstr "更早..."

#: ../src/app/qml/documentPage/SharePage.qml:23
msgid "Share to"
msgstr "分享到"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "排序设置"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "按日期排序（最近优先）"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "按名称排序（A-Z）"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "按大小排序（最小优先）"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "倒序"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:51
#: ../src/app/qml/textView/TextView.qml:43
msgid "Loading..."
msgstr "加载中..."

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:55
msgid "LibreOffice text document"
msgstr "LibreOffice 文本文档"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:57
msgid "LibreOffice spread sheet"
msgstr "LibreOffice 电子表格"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:59
msgid "LibreOffice presentation"
msgstr "LibreOffice 演示"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:61
msgid "LibreOffice Draw document"
msgstr "LibreOffice 绘图文档"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:63
msgid "Unknown LibreOffice document"
msgstr "未知 LibreOffice 文档"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:65
msgid "Unknown type document"
msgstr "未知类型文档"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:85
msgid "Go to position..."
msgstr "跳转到..."

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:100
#: ../src/app/qml/pdfView/PdfView.qml:228
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:63
msgid "Disable night mode"
msgstr "禁用夜间模式"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:100
#: ../src/app/qml/pdfView/PdfView.qml:228
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:63
msgid "Enable night mode"
msgstr "开启夜间模式"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "跳转"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "请选择 %1 至 100% 之间的位置"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:62
#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "跳转！"

#: ../src/app/qml/loView/LOViewPage.qml:167
msgid "LibreOffice binaries not found."
msgstr "未找到 LibreOffice 二进制文件。"

#: ../src/app/qml/loView/LOViewPage.qml:170
msgid "Error while loading LibreOffice."
msgstr "加载 LibreOffice 错误。"

#: ../src/app/qml/loView/LOViewPage.qml:173
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"文档未加载。\n"
"请求的文档可能已损坏或有密码保护。"

#: ../src/app/qml/loView/LOViewPage.qml:228
msgid "This sheet has no content."
msgstr "这页没有内容。"

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: ../src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "由 LibreOfficeKit 驱动"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: ../src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "%1 页"

#: ../src/app/qml/loView/ZoomSelector.qml:122
msgid "Fit width"
msgstr "合适的宽度"

#: ../src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "合适的高度"

#: ../src/app/qml/loView/ZoomSelector.qml:124
msgid "Automatic"
msgstr "自动的"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: ../src/app/qml/pdfView/PdfContentsPage.qml:31
#: ../src/app/qml/pdfView/PdfView.qml:153
msgid "Contents"
msgstr "目录"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: ../src/app/qml/pdfView/PdfPresentation.qml:51
#: ../src/app/qml/pdfView/PdfView.qml:56
#, qt-format
msgid "Page %1 of %2"
msgstr "第 %1 页 共 %2 页"

#: ../src/app/qml/pdfView/PdfView.qml:203
msgid "Search"
msgstr "搜索"

#: ../src/app/qml/pdfView/PdfView.qml:213
msgid "Go to page..."
msgstr "跳转页面..."

#: ../src/app/qml/pdfView/PdfView.qml:221
msgid "Presentation"
msgstr "演示"

#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "跳转页面"

#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "请选择第 1 页至第 %1 页之间的一页"

#: ../src/app/qml/ubuntu-docviewer-app.qml:114
msgid "File does not exist."
msgstr "文件不存在。"

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: ../src/plugin/file-qml-plugin/docviewerutils.cpp:111
#, qt-format
msgid "copy %1"
msgstr "副本 %1"

#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:1
msgid "Document Viewer"
msgstr "文档阅读器"

#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:2
msgid "documents;viewer;pdf;reader;"
msgstr "文档;查看;PDF;阅读;"

#~ msgid "File does not exist"
#~ msgstr "文件不存在"
