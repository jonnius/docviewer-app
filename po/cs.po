# Czech translation for ubuntu-docviewer-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-docviewer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-docviewer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-02-05 23:40+0100\n"
"PO-Revision-Date: 2020-06-17 21:25+0000\n"
"Last-Translator: Milan Korecký <milan.korecky@gmail.com>\n"
"Language-Team: Czech <https://translate.ubports.com/projects/ubports/"
"docviewer-app/cs/>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-05 07:48+0000\n"

#: ../src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "Některé ze zadaných argumentů nejsou platné."

#: ../src/app/qml/common/CommandLineProxy.qml:58
msgid "Open ubuntu-docviewer-app displaying the selected file"
msgstr "Otevřít ubuntu-docviewer-app a zobrazit v něm vybraný soubor"

#: ../src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "Celá obrazovka"

#: ../src/app/qml/common/CommandLineProxy.qml:71
msgid "Open ubuntu-docviewer-app in pick mode. Used for tests only."
msgstr ""
"Otevřít ubuntu-docviewer-app v režimu výběru. Používá se pouze pro testování."

#: ../src/app/qml/common/CommandLineProxy.qml:77
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running ubuntu-docviewer-app"
msgstr ""
"Zobrazit dokumenty z dané složky namísto  ~/Dokumenty.\n"
"Je třeba, aby popis umístění existoval už před spuštěním ubuntu-docviewer-app"

#: ../src/app/qml/common/DetailsPage.qml:26
#: ../src/app/qml/loView/LOViewDefaultHeader.qml:107
#: ../src/app/qml/pdfView/PdfView.qml:235
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:69
msgid "Details"
msgstr "Podrobnosti"

#: ../src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "Soubor"

#: ../src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "Poloha"

#: ../src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "Velikost"

#: ../src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "Vytvořeno"

#: ../src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "Naposledy upraveno"

#: ../src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "MIME typ"

#: ../src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "Chyba"

#: ../src/app/qml/common/ErrorDialog.qml:26
#: ../src/app/qml/common/PickImportedDialog.qml:54
#: ../src/app/qml/common/RejectedImportDialog.qml:38
#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:31
#: ../src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "Ukončit"

#: ../src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "Naimportováno vícero dokumentů"

#: ../src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "Vyberte, který se má otevřít:"

#: ../src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "Nepodporovaný soubor"
msgstr[1] "Nepodporované soubory"
msgstr[2] "Nepodporovaných souborů"

#: ../src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "Následující dokument nebyl naimportován:"
msgstr[1] "Následující dokumenty nebyly naimportovány:"
msgstr[2] "Následujících dokumentů nebylo naimportováno:"

#: ../src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "Neznámý typ souboru"

#: ../src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"Tento soubor není podporován. \n"
"Chcete ho otevřít jako obyčejný text?"

#: ../src/app/qml/common/UnknownTypeDialog.qml:38
#: ../src/app/qml/documentPage/DeleteFileDialog.qml:55
#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:28
#: ../src/app/qml/loView/LOViewGotoDialog.qml:55
#: ../src/app/qml/pdfView/PdfView.qml:180
#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "Storno"

#: ../src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "Ano"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: ../src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 GB"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: ../src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 MB"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: ../src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 KB"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: ../src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 bajtů"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "Smazat soubor"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "Smazat %1 soubor"
msgstr[1] "Smazat %1 soubory"
msgstr[2] "Smazat %1 souborů"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:41
#: ../src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "Opravdu chcete tento soubor natrvalo smazat?"
msgstr[1] "Opravdu chcete tyto soubory natrvalo smazat?"
msgstr[2] "Opravdu chcete tyto soubory natrvalo smazat?"

#: ../src/app/qml/documentPage/DeleteFileDialog.qml:61
#: ../src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:53
msgid "Delete"
msgstr "Smazat"

#: ../src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "Sdílet"

#: ../src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr "Nenalezeny žádné dokumenty"

#: ../src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr ""
"Připojte zařízení k počítači a přetáhněte soubory do složky Dokumenty nebo "
"vložte vyměnitelné médium s dokumenty."

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:103
#, qt-format
msgid "Today, %1"
msgstr "Dnes, %1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:108
#, qt-format
msgid "Yesterday, %1"
msgstr "Včera, %1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:115
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:134
msgid "yyyy/MM/dd hh:mm"
msgstr "yyyy/MM/dd hh:mm"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: ../src/app/qml/documentPage/DocumentListDelegate.qml:128
msgid "dddd, hh:mm"
msgstr "dddd, hh:mm"

#: ../src/app/qml/documentPage/DocumentPage.qml:23
#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:3
msgid "Documents"
msgstr "Dokumenty"

#: ../src/app/qml/documentPage/DocumentPageDefaultHeader.qml:29
msgid "Search..."
msgstr "Hledat…"

#: ../src/app/qml/documentPage/DocumentPageDefaultHeader.qml:36
msgid "Sorting settings..."
msgstr "Nastavení řazení…"

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:41
msgid "Switch to single column list"
msgstr "Přepnout do seznamu s jedním sloupcem"

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:41
msgid "Switch to grid"
msgstr "Přepnout do mrížky"

#: ../src/app/qml/documentPage/DocumentPagePickModeHeader.qml:49
msgid "Pick"
msgstr "Vybrat"

#: ../src/app/qml/documentPage/DocumentPageSearchHeader.qml:27
msgid "Back"
msgstr "Zpět"

#: ../src/app/qml/documentPage/DocumentPageSearchHeader.qml:47
msgid "search in documents..."
msgstr "hledat v dokumentech…"

#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:40
msgid "Select None"
msgstr "Nevybrat žádný"

#: ../src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:40
msgid "Select All"
msgstr "Vybrat vše"

#: ../src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr "Nenalezeny žádné dokumenty"

#: ../src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr "Ověřte, že váš dotaz je správně zapsaný anebo zkuste jiný."

#: ../src/app/qml/documentPage/SectionHeader.qml:13
msgid "Today"
msgstr "Dnes"

#: ../src/app/qml/documentPage/SectionHeader.qml:16
msgid "Yesterday"
msgstr "Včera"

#: ../src/app/qml/documentPage/SectionHeader.qml:19
msgid "Earlier this week"
msgstr "Začátkem tohoto týdne"

#: ../src/app/qml/documentPage/SectionHeader.qml:22
msgid "Earlier this month"
msgstr "Začátkem tohoto měsíce"

#: ../src/app/qml/documentPage/SectionHeader.qml:24
msgid "Even earlier..."
msgstr "Ještě dříve…"

#: ../src/app/qml/documentPage/SharePage.qml:23
msgid "Share to"
msgstr "Sdílet s"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "Nastavení řazení"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "Řadit dle data (nejnovější jako první)"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "Dle názvu (A-Z)"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "Řadit dle velikosti (nejmenší první)"

#: ../src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "Opačné pořadí"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:51
#: ../src/app/qml/textView/TextView.qml:43
msgid "Loading..."
msgstr "Načítání…"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:55
msgid "LibreOffice text document"
msgstr "LibreOffice textový dokument"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:57
msgid "LibreOffice spread sheet"
msgstr "LibreOffice tabulky"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:59
msgid "LibreOffice presentation"
msgstr "LibreOffice prezentace"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:61
msgid "LibreOffice Draw document"
msgstr "LibreOffice kresba"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:63
msgid "Unknown LibreOffice document"
msgstr "Neznámý LibreOffice dokument"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:65
msgid "Unknown type document"
msgstr "Neznámý typ dokumentu"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:85
msgid "Go to position..."
msgstr "Přejít na pozici…"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:100
#: ../src/app/qml/pdfView/PdfView.qml:228
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:63
msgid "Disable night mode"
msgstr "Vypnout noční režim"

#: ../src/app/qml/loView/LOViewDefaultHeader.qml:100
#: ../src/app/qml/pdfView/PdfView.qml:228
#: ../src/app/qml/textView/TextViewDefaultHeader.qml:63
msgid "Enable night mode"
msgstr "Zapnout noční režim"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "Přejít na pozici"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "Vybrat pozici mezi 1% a 100%"

#: ../src/app/qml/loView/LOViewGotoDialog.qml:62
#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "PŘEJÍT!"

#: ../src/app/qml/loView/LOViewPage.qml:167
msgid "LibreOffice binaries not found."
msgstr "Nenalezeny spustitelné soubory s LibreOffice."

#: ../src/app/qml/loView/LOViewPage.qml:170
msgid "Error while loading LibreOffice."
msgstr "Chyba při načítání LibreOffice."

#: ../src/app/qml/loView/LOViewPage.qml:173
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"Dokument nenačten.\n"
"Požadovaný dokument může být poškozený anebo chráněný heslem."

#: ../src/app/qml/loView/LOViewPage.qml:228
msgid "This sheet has no content."
msgstr "Tento list nemá žádný obsah."

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: ../src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "Založeno na LibreOfficeKit"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: ../src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "List%1"

#: ../src/app/qml/loView/ZoomSelector.qml:122
msgid "Fit width"
msgstr "Přizpůsobit šířce"

#: ../src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "Přizpůsobit výšce"

#: ../src/app/qml/loView/ZoomSelector.qml:124
msgid "Automatic"
msgstr "Automaticky"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: ../src/app/qml/pdfView/PdfContentsPage.qml:31
#: ../src/app/qml/pdfView/PdfView.qml:153
msgid "Contents"
msgstr "Obsah"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: ../src/app/qml/pdfView/PdfPresentation.qml:51
#: ../src/app/qml/pdfView/PdfView.qml:56
#, qt-format
msgid "Page %1 of %2"
msgstr "Strana %1 z %2"

#: ../src/app/qml/pdfView/PdfView.qml:203
msgid "Search"
msgstr "Hledat"

#: ../src/app/qml/pdfView/PdfView.qml:213
msgid "Go to page..."
msgstr "Přejít na stranu…"

#: ../src/app/qml/pdfView/PdfView.qml:221
msgid "Presentation"
msgstr "Prezentace"

#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "Přejít na stránku"

#: ../src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "Vybrat stranu mezi 1 a %1"

#: ../src/app/qml/ubuntu-docviewer-app.qml:114
msgid "File does not exist."
msgstr "Soubor neexistuje."

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: ../src/plugin/file-qml-plugin/docviewerutils.cpp:111
#, qt-format
msgid "copy %1"
msgstr "kopie %1"

#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:1
msgid "Document Viewer"
msgstr "Prohlížeč dokumentů"

#: /tmp/lok-qml-async-imageprovider-build/po/com.ubuntu.docviewer.desktop.in.in.h:2
msgid "documents;viewer;pdf;reader;"
msgstr "dokumenty;prohlížeč;pdf;čtečka;"

#~ msgid "File does not exist"
#~ msgstr "Soubor neexistuje"

#~ msgid "No document found"
#~ msgstr "Nebyl nalezen žádný dokument"
